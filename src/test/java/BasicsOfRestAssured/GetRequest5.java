package BasicsOfRestAssured;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.matcher.ResponseAwareMatcher;
import io.restassured.response.Response;

public class GetRequest5 {

		@Test
	public void restAssuredGet()
	{
	String firstnameVal = RestAssured
		.get("https://restful-booker.herokuapp.com/booking/10")
		.then()
		.statusCode(200)
		.statusLine("HTTP/1.1 200 OK")
		.body("firstname",Matchers.equalTo("Mark"))
		.body("bookingdates.checkin", Matchers.equalTo("2016-11-14"))
		.extract()
		.jsonPath()
		.getString("firstname");
		System.out.println(firstnameVal);
		
		
		
		
		
			
	}
}
