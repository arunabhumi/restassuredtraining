package BasicsOfRestAssured;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.matcher.ResponseAwareMatcher;
import io.restassured.response.Response;

public class GetRequest3 {

		@Test
	public void restAssuredGet()
	{
		RestAssured
		.get("https://restful-booker.herokuapp.com/booking/10")
		.then()
		.statusCode(200)
		.statusLine("HTTP/1.1 200 OK")
		.body("firstname", Matchers.equalTo("Mary"))
		.body("bookingdates.checkin", Matchers.equalTo("2017-03-15"));
		
		
	}
}
